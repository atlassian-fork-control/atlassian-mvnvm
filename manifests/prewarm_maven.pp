# Pre install a version of maven
define mvnvm::prewarm_maven(
  Stdlib::Absolutepath $home = $::root_home,
) {
  exec { "prewarm maven ${name}":
    command     => "${mvnvm::bin_dir}/mvn --download-only --mvn-version ${name}",
    environment => ["HOME=${home}"],
  }
  -> file { "${home}/.mvnvm/apache-maven-${name}-bin.zip" :
    ensure => absent,
  }
}
